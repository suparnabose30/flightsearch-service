package com.suparna.flightsearchservice.repository;

import com.suparna.flightsearchservice.model.FlightSearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightSearchRepository extends JpaRepository<FlightSearch,Long> {

}
